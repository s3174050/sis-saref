"""
Note:
    Please replace 'path/to/household.csv' in line 83 with the actual path to your CSV file.
"""




import os
import sys
import pandas as pd
import rdflib
from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF, RDFS, XSD

def load_data(file_path):
    try:
        df = pd.read_csv(file_path)
        print("File loaded successfully.")
        print(df.head())
        return df
    except Exception as e:
        print(f"Error loading file: {e}")
        return None

def create_rdf_graph(df):
    if df is None:
        print("DataFrame is None, exiting...")
        return None

    # Define namespaces
    SAREF = Namespace("https://saref.etsi.org/core/v3.1.1/")
    EX = Namespace("http://example.org/")

    # Create a new RDF graph
    g = Graph()
    g.bind("saref", SAREF)
    g.bind("ex", EX)

    def convert_to_boolean(value):
        if str(value).lower() in ['true', '1']:
            return True
        elif str(value).lower() in ['false', '0']:
            return False
        else:
            return None

    # Iterate over the rows and columns of the dataframe
    row_count = len(df)
    for index, row in df.iterrows():
        if index % 5000 == 0:
            print(f"Processing row {index}/{row_count}...")

        timestamp = row['utc_timestamp']
        interpolated = convert_to_boolean(row['interpolated'])

        for col in df.columns:
            if col in ['utc_timestamp', 'cet_cest_timestamp', 'interpolated']:
                continue

            device_uri = EX[f"Device/{col}"]
            measurement_uri = EX[f"Measurement/{index}_{col}"]

            # Add triples for the device using SAREF ontology
            g.add((device_uri, RDF.type, SAREF.Device))
            g.add((device_uri, RDFS.label, Literal(col, datatype=XSD.string)))

            # Add triples for the measurements using SAREF ontology
            g.add((measurement_uri, RDF.type, SAREF.Measurement))
            g.add((measurement_uri, SAREF.hasValue, Literal(row[col], datatype=XSD.float)))
            g.add((measurement_uri, SAREF.isMeasuredIn, Literal("kWh", datatype=XSD.string)))
            g.add((measurement_uri, SAREF.relatesToProperty, device_uri))
            g.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))

            if interpolated is not None:
                g.add((measurement_uri, EX.interpolated, Literal(interpolated, datatype=XSD.boolean)))

    print("RDF graph created successfully.")
    return g

def main():
    if len(sys.argv) != 2:
        print(r"Usage: python trans.py path/to/household.csv")
        sys.exit(1)

    csv_file_path = sys.argv[1]
    output_file_path = r"graph.ttl"

    # Load data
    df = load_data(csv_file_path)
    if df is None:
        print("Failed to load data. Exiting...")
        return

    # Create RDF graph
    rdf_graph = create_rdf_graph(df)
    if rdf_graph is None:
        print("Failed to create RDF graph. Exiting...")
        return

    try:
        rdf_graph.serialize(destination=output_file_path, format='turtle')
        print(f"RDF graph has been serialized to {os.path.abspath(output_file_path)}")
    except Exception as e:
        print(f"Error serializing RDF graph: {e}")

    print("Script completed successfully. Exiting now.")

if __name__ == "__main__":
    main()
    print("Main function completed.")
    sys.exit(0)
